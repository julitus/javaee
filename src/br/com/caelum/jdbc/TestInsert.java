package br.com.caelum.jdbc;

import java.util.Calendar;

public class TestInsert {

	public static void main(String[] args) {
		
		/*Contact contacto = new Contact();
		contacto.setNombre("Julio");
		contacto.setEmail("jces@gmail.com");
		contacto.setDireccion("Calle los Olivos 131");
		contacto.setFechaNac(Calendar.getInstance());
		
		ContactDAO dao = new ContactDAO();
		dao.adiciona(contacto);
		
		System.out.println("Guardado!");*/
		
		Contact contacto = new Contact();
		contacto.setId(2L);
		contacto.setNombre("Cesar");
		contacto.setEmail("jces@gmail.com");
		contacto.setDireccion("Calle los Olivos 131");
		contacto.setFechaNac(Calendar.getInstance());
		
		ContactDAO dao = new ContactDAO();
		dao.altera(contacto);
		
		System.out.println("Alterado!");
	}

}
