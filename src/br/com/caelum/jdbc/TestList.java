package br.com.caelum.jdbc;

import java.util.List;

public class TestList {

	public static void main(String[] args) {
	
		ContactDAO dao = new ContactDAO();
		List<Contact> contactos = dao.getLista();
		for (Contact contacto : contactos){
			System.out.println("Nombre: " + contacto.getNombre());
			System.out.println("Email: " + contacto.getEmail());
			System.out.println("Direccion: " + contacto.getDireccion());
			System.out.println("Fecha de Nac: " + contacto.getFechaNac().getTime() + "\n");
			
		}
		
	}

}
