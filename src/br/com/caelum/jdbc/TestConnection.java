package br.com.caelum.jdbc;

import java.sql.Connection;
import java.sql.SQLException;

public class TestConnection {

	public static void main(String[] args) throws SQLException {
		Connection conex = new ConnectionFactory().getConnection();
		System.out.println("conexion abierta!");
		conex.close();
	}

}
